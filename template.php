<?php
/**
 * Custom search form.
 */
function sport_form_alter(&$form, &$form_state, $form_id){
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#attributes']['class'] = array('search-input');
    $form['actions']['submit']['#type'] = 'image_button';
    $form['actions']['submit']['#src'] = drupal_get_path('theme', 'sport') . '/images/search.png';
    $form['actions']['submit']['#attributes']['class'] = array('search-button');
  }
}